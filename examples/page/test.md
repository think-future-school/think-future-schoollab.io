---
title: AWESOME Roses
subtitle: 
tags: ["bigimg", "salebarn"]
bigimg: [{src: "https://www.finegardening.com/app/uploads/sites/finegardening.com/files/images/spotlight-collection/untitled6.png", desc: "Genetics and Breeding"}, {src: "https://hedgerowrose.com/wp-content/uploads/2015/03/Propagating-Roses.png", desc: "Taking Rose Cuttings"}, {src: "http://2.bp.blogspot.com/-YDsHedOwl6Y/UOWRkc6jcoI/AAAAAAAABHw/pog8jJuKxoA/s1600/bloom.jpg", desc: "Container Gardening"}, {src: "https://pics.davesgarden.com/pics/2008/12/20/billgrubbs/299abf.jpg", desc: "Aeration Of Cuttings"}, {src: "http://www.premier-roses.com/sites/default/files/basic-sidebar-images/premier-roses-watering.jpg", desc: "Humidification of Micrclimate"}]
---


- [Introduction](#introduction)
- [Genetics](#genetics)
- [Cuttings](#cuttings)
- [Containers](#containers)
- [Aeration](#aeration)
- [Humidification of Micrclimate](#humidification)


## Introduction 

For an introduction to garden roses, START by reading the [Wikipedia *garden rose* entry](https://en.wikipedia.org/wiki/Garden_roses) an skim over the [larger, complete *ROSE* entry](https://en.wikipedia.org/wiki/Rose) ... if you are curious, you will probably want to grab some keywords from that with your own ever-expanding interwebs search ... but START with the big picture, before wasting time watching videos about using potatoes for propagating because video grazing just leads to watching stuff like mating tapirs.

Roses, just for gardening, are a serious global industry with what is comfortably more than one billion dollars worth of annual sales, year after year ... about one fourth of that coming from the US ... there are myriad reasons for staying power of roses. The larger world of people having something to do with garden roses illustrates the industry, the history and the science that develops out of several thousand years of continual imprpovement in [dedicated, systematic rose breeding efforts](https://ccuh.ucdavis.edu/sites/g/files/dgvnsk1376/files/inline-files/tom%20keith%20rose%20breeding%202008.pdf) and [cultivation in China, Persia, Mesopotamia, Egypt, Greece and Rome, long before the beginning of the Christian era.](https://actahort.org/books/424/424_43.htm) ... at first blush, interest in growing roses might be a little like operating a worm ranch or tropical fish tank or breeding hunting dogs, ie something that might bewilder or infuriate those with no interest and, although enjoyable and possibly financially rewarding enough to pull its own weight, not exactly an enterprise that a professional with valuable skills does to fund one's family, but instead an avocation that one does out of love for the sport and community of people fascinated with a topic in  Life or Nature. 

The topic of roses therefore is about everything else, not just about roses or dreams of big money in rose cultivation -- instead, it's all about connections to botantists, horticulturalists and others with serious interest through the ages who have cared about or have been inspired by roses ... the topic of roses is really about the STORY of roses and the storytellers.

The Whore's Radish project is a bit of a story-completion game, not exactly about mustards and horseradish ... kind of ...  but mostly it's about the stories, in which YOUR creativity and insights help everyone in the game, but especially those who choose to use their head and play. You can participate and even make the game better ... if you want to play, you can [fork this repository and improve the process of curating/annotating lists like this](https://engrco.gitlab.io/whoresradish/).

Our use of pictures is not accidental ... we primarily search for content through images -- we recognize that visuals or semiotic signs have ways of provoking thought before humans can even vocalize a response or come up with an interesting caption, ie visuals are more direct ... if you ever use social media, you will probably tend to scroll through the pics OR spend more time looking images from a search engine before following links. Search starts with something simple like the [#Roses hashtag on Instagram](https://www.instagram.com/explore/tags/roses/) ... it's not *just* to see pretty pictures ... some of those postings will provoke reactions and have comment-trails that lead you down a rabbithole with *convulsions in fits of unstoppable laughter* ... 

## Genetics

Texas A&M has an excellent set of guides for those who are thinking about [getting started in rose breeding](https://roses.tamu.edu/rose-breeding-for-beginners/) including [flower anatomy](https://drive.google.com/file/d/1VqJKLib9GpVgx0t1OLtAGoARq9I2LwKI/view), [emasculation and pollen collection](https://drive.google.com/file/d/1TzKNDC4Ciu9g4mbrTFQB1_4uFE3tQM0Z/view), [pollination](https://drive.google.com/file/d/1xI8Ns-LKCQzLowCtmYPSjhpsEeR6NDMR/view), [processing rose seeds](https://drive.google.com/file/d/1LZGJ1aLypBxzO67Z8wNYiP3b_O1DSN_h/view), [stratification of seeds](https://drive.google.com/file/d/1__L8YlQq39dnX5hlB_wjD0DmDZMr3dSM/view), [germination of seeds](https://drive.google.com/file/d/1v0yViBi1NIOXwzBmuZ8bv4_12KhOlXmG/view), [selection and breeding strategies](https://drive.google.com/file/d/13I6BsVQpZpthQUHhEUe7hnj05LfZ4acT/view). Of course, you can go further, join Texas A&M's rose breeding listserv, etc ... but VERY few people will have the patience for [a serious rose breeding hobby and blog](https://sproulroses.blogspot.com/) or will try to [push the envelope](http://pushingtheroseenvelope.blogspot.com/) or curating a [serious rose-breeding garden over a long term](http://rosomanes.blogspot.com/), but this level of meticulous attention to scientific detail is a great example to inspire others.

## Cuttings

The [best approach is relatively simple and cheap](https://www.1001-landscaping-ideas.com/growing-rose-cuttings.html) ... don't complicate it with YouTube tricks, additives, special chemicals or fertilizers -- just look a lot of articles, THINK about what you have read, then give it your best shot and try to observe closely what seems to works, what doesn't, then adjust your approach and get better. 

The BASICS matter ... start off by using a CLEAN, sterile, extremely sharp pruning shears ... cut at 45 degree angle ... be sure to keep the cuttings out of the Sun and get the cuttings into a moist media ASAP ... moist is not WET ... the media should have good drainage for good root oxygen, eg a very-very-well rotted compost-soil mix with plenty of perlite might be ideal ... but READ what others have used and experiment, experiment more and LEARN in order to experiment and LEARN more effectively in the future, ie *why are you bothering with this anyway?*  If your objective is just nice landscaping, you probably hire a professional ... it's like fishing, how much your fish cost you and whether you really want an occupation selling your catch to restaurants ... ENJOY everything about the process, especially learning the small things ... or leave cuttings and propagation to somebody else.


## Containers

Containers are a really big deal in gardening ... being able to move plants around opens up a lot of options ... generally speaking, avoid large unmovable fixtures, like raised beds, until you are darned sure that you know exactly what is going to work best and where.

Yes, plants are inherently sessile living things that you might believe ultimately must belong in a fixed space ... HOWEVER, especially for things like propagation and just getting to the next phase that might be a few months or less away, nothing is better than mobility ... especially if you want to inspect or work on the plants several times or might need to make adjustments to get more sun or a more appropriate temperature or humidity profile.

## Aeration

Do NOT forget that roots might want moisture, but they moistly NEED oxygen. Develop a method which allows your living cuttings that you are hoping to root a way to breathe ... AND keep the enviroment around them moist.

## Humidification

Find a way to mist the plants that you are attempting to propagate. Be create. Scour the interwebs for ideas. Experiment. Experiment. Experiment, ie *why are you bothering with this anyway?*
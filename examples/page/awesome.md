---
title: Curation / Annotation is AWESOME
subtitle: Readers are Leaders; Leaders Induce Others To Read
comments: false
---

This is an ongoing project in curation and annotation. 

In a META sense, the process of developing repositories like this is in itself a learning experiment, a developing work-in-progress, something we don't have right just yet. Anyone can and probably should do it much better. Hopefully, you will decide to implement something like that. *EXTENSIBILY of ordinary, but useful PRACTICAL ideas [for billions] is the whole point of open source.*

In order to extend or improve this project ... it's kind of simple, not yet simple enough but ... just use/extend your own knowledge of [GitLab-flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html), something which is necessary for the asynchrononous work which we increasing find in the leaner workflows such as Git-centric, issues-driven communication structures, then [pick up a basic awareness of how the Hugo framework works](https://gohugo.io/documentation/) *and if you know GoLang, then improve Hugo, but that's optional* ... of course, you will need to have a fair bit of [basic Git administration awareness](https://git-scm.com/book/en/v2) and also knowledge of details like the [Gitlab-specific material on Git Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html) implemented in Rails and, in particular, a solid knowledge [how to manage your own repos of GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) ... 

As soon as you have the preliminaries, fork [the Whore's Radish reposiotry](https://gitlab.com/engrco/whoresradish) for your own Gitlab account, OR, maybe you only care about something like coffee plants in particular -- then, just clone [coffee.md](https://gitlab.com/engrco/whoresradish/-/blob/master/content/page/ch12/coffee.md) or even just copy-paste the parts of it you want ... the practicality of this is that you can pick up the parts that you want ... and the whole framework is FAST loading and DEAD SIMPLE -- it does not advertising or tracking analytics or a support team. 

So, that's it ... get your fingernails in the dirt and start enjoying it ... **[FORK this repository](https://gitlab.com/engrco/whoresradish)**, study and improve the repository's structure, then start annotating and curating your own repositories of AWESOME lists ... but, mostly, find better ways to encourage active, immersive reading ... develop something that is even more AWESOME than curated, annotated lists.

*It is really just ***that*** simple ... and yeah, it should be much simpler. After you work will this framework for a bit, you will probably have better ideas on why it matters or how to do it better, simpler ... so just dig in and* **MAKE IT HAPPEN!**

## The Whore's Radish game is all about EXTENDING leadership.

Leadership comes from independent thought and the processes of organizing information and sharing that organization with others. 

Active, immersive READING informs opinions ... that's why we push the annotation and curation of AWESOME lists  ... because ***annotating and curating a list for others helps the annotator / curator LEARN more deeply, profoundly than in just reading the material***. There are so many examples of extremely well-written, and often well-illustrated material that are far deeper, better, more useful than almost any video ... you could start with something like [a list of 48 medicinal herbs by Balcony Garden Web](https://balconygardenweb.com/best-medicinal-plants/) ... but the key of this whole project is the **PROMOTION OF MORE READING** ... reader are leaders; people who watch are followers.

When a person actively, immersively ***reads*** a lot of material ... important processing and thinking skills develop in ingesting greater quantities of more information ... it is most importantly about the structure of written information. In ten minutes of active reading, any decent reader will be able to discern more value, more information than what is presented in the best two-hour documentary ... the key of this whole project is the **PROMOTION OF MORE READING** ... more leadership, less watching would be AWESOME!
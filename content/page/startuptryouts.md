---
title: StartupTryouts
subtitle: 
comments: false
---
 
Anything new, created anywhere in the world - from a tiny gadget to a Mars mission rocket - is a startup. In other words, any project aiming to deliver a product or a service to real-life customers can be classified as a real-life Project-Based Learning activity.
 
StartupTryouts is the program by OnwardTrek for teaching students how to launch a new enterprise — whether it’s a tech start-up, a small business, an initiative within an organization or a new Kickstarter project.
 
Eric Ries once said, “Startup success can be engineered by following the process, which means it can be learned, which means it can be taught”.
 
StartupTryouts goal is to create opportunities for students to establish and practice their core success skills: self-direction, risk-taking, adaptability, creativity, innovation, critical thinking and problem solving. An entrepreneurial mindset is defined as the set of attitudes, skills and behaviors that students need to succeed academically and professionally. Through mastering these skills students can develop and present their startups to teachers, parents, local entrepreneurs and investors, providing them with the most challenging, and therefore most valuable learning experience.

### Curriculum

1. Orientation. Why build a startup.
2. Traits of an entrepreneur. Technical/non-technical founders.
3. Mechanics of innovation. From zero to one.
4. How software startups work. School-based startups.
5. Evaluating startup ideas. Lean startup process.
6. Kanban and Scrum. Project Management methodologies.
7. Git version control system. 
8. How to work together. Building startup team culture.
9. How to talk to users and customers.
10. How to set KPIs and goals. Analytics for startups.
11. How to plan an MVP. All about pivoting.
12. How to launch (again and again). Growth for startups.
13. Improving conversion rates. Startup pricing 101.
14. Nine business models. Website and messaging.

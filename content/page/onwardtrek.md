---
title: OnwardTrek
subtitle: Bettering student opportunities for self-driven projects and activities
comments: false
---
 
### OnwardTrek Technology platform
 
[OnwardTrek](https://onwardtrek.com/) supports student clubs and a wide variety of other student-directed Project-Based Learning activities by creating a student-driven market of ideas, interests and projects. It fulfills the need for creating an inspiring supporting environment within a school for students discovering their individual passions and pursuing their individual learning pathways.
 
### Learning Pathways
 
Delivered as a classroom or a school wide workshop, Learning Pathways is a design thinking self-assessment methodology for helping students discover their interests, define learning needs and develop projects for achieving their personal learning goals.
 
### Team-alignment
 
One of the key objectives is the discovery of mutual interests and the team formation process between students within their class and school. A school-wide campaign has the potential to maximize the synergy and collaboration between students and teachers in developing student-driven activities and challenge-driven learning culture.
 
### Student portfolio
 
As students pursue their objectives within a host of projects their personal history of experiences are documented in their student portfolio, to be shared with parents, colleges and internship work programs.



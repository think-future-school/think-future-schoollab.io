---
title: About
subtitle: We are exploring what a school can look like when innovations in education are intentionally practiced.
comments: false
---

Our goal is to ask questions that can lead to discussions and down the line, hopefully, to better solutions in the world of K-12 education.
